{-# LANGUAGE RankNTypes #-}

module Pipes.Proxy.Extra where


import Pipes (lift)
import Pipes.Core (Proxy, respond, request)


type BiPipe a' a b' b m r = a -> Proxy a' a b' b m r
type BelowPipe' a b m r = forall x'. BiPipe x' a x' b m r
type AbovePipe' a' b' m r = forall y. BiPipe a' y b' y m r



-- | Generate bidirectional Pipe.
bipipeM :: Monad m
       => (a -> m b) -- ^ from upstream to downstream
       -> (b' -> m a') -- ^ from downstream to upstream
       -> BiPipe a' a b' b m r
bipipeM f g = next
  where
    next a = lift (f a) >>= respond >>= (lift . g) >>= request >>= next

bipipe :: (Monad m, Applicative m)
       => (a -> b)
       -> (b' -> a')
       -> BiPipe a' a b' b m r
bipipe f g = bipipeM (pure . f) (pure . g)


-- | `belowM f == bipipeM f pure`
belowM :: Monad m => (a -> m b) -> BelowPipe' a b m r
belowM f = bipipeM f pure
below :: (Monad m, Applicative m) => (a -> b) -> BelowPipe' a b m r
below f = bipipe f id

-- | `aboveM f == bipipeM pure f`
aboveM :: Monad m => (b -> m a) -> AbovePipe' a b m r
aboveM = bipipeM pure
above :: (Monad m, Applicative m) => (b -> a) -> AbovePipe' a b m r
above = bipipe id
