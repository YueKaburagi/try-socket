{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE OverloadedStrings #-}



-- | Pipesの型の示すところ
--  ライブラリとその Tutorial を見てすぐに理解できなかった箇所
--
--  (a' -> Client' a' a m r); as (a' -> Proxy a' a x' x m r)
--  これは始点で a' が供給され
--  それを内部で a' -> m a して
--  a を上流に返すことで次の a' が貰える (request a)
--  それをまた a' -> m a して〜 とする箇所

-- respond あげるからつくって
-- request つくったから次ちょうだい


module Main where

import Relude hiding (Proxy)

import Control.Exception (bracket, try, throwIO)
import Control.Monad (unless)
import Data.Bifunctor (first)
import Data.List.Split (splitOn)
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Foreign as T
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BS
import qualified Data.ByteString.Char8 as C8
import qualified Data.ByteString.Lazy as BSL
import Network.Socket as Socket
import qualified Network.Socket.ByteString as SocketBS
import qualified GHC.IO.Exception as G
import Options
import Pipes hiding (next)
import Pipes.Core
import Pipes.Network.TCP
--import Pipes.Network.TCP.Safe
import Pipes.Proxy.Extra 
import System.IO (isEOF, stdout)
import System.Posix.Files (removeLink)


data SocketTypeIndex = STUnix | STIP4
                deriving (Show, Eq, Ord, Enum, Bounded)

data MainOptions = MainOptions
  { optSocketType :: SocketTypeIndex
  }
instance Options MainOptions where
  defineOptions = MainOptions
    <$> defineOption (optionType_enum "type")
    (\o -> o { optionLongFlags = ["type"]
             , optionDefault = STUnix
             , optionDescription = ""
             } )
data UNIXSockOptions =
  UNIXSockOptions
  { optUnixSockAddr :: String
  }
instance Options UNIXSockOptions where
  defineOptions =
    UNIXSockOptions
    <$> simpleOption "sock" "/tmp/sock-test.sock" "UNIX socket name."

data INETSockOptions =
  INETSockOptions
  { optInetHostAddr :: HostAddress
  , optInetPortNumber :: PortNumber
  }
instance Options INETSockOptions where
  defineOptions =
    INETSockOptions
    <$> defineOption (optionType "ip" ipdefault str2ip ip2str)
    (\o -> o { optionLongFlags = ["ip"]
             , optionDescription = "IP address."
             } )
    <*> defineOption (optionType "port" 33333 (first T.unpack . readEither) show)
    (\o -> o { optionLongFlags = ["port"]
             , optionDescription = "Port number."
             } )


str2ip :: String -> Either String HostAddress
str2ip str =
  case traverse readEither $ splitOn "." str of
    Right [a,b,c,d] -> pure $ tupleToHostAddress (a,b,c,d)
    Right _ -> Left "IP4 only"
    Left err -> Left $ T.unpack err
ip2str :: HostAddress -> String
ip2str addr =
  case hostAddressToTuple addr of
    (a,b,c,d) -> show a <> "." <> show b <> "." <> show c <> "." <> show d
ipdefault :: HostAddress
ipdefault = tupleToHostAddress (0x7F,0,0,0x01)


entry :: (AddrInfo -> IO Socket) -> (Socket -> IO a) -> MainOptions -> MainOptions -> [String] -> IO a
entry f g _ _ _ =
  withConnectedSock (f loopback) g


entryIP4Sock :: MainOptions -> INETSockOptions -> [String] -> IO ()
entryIP4Sock _ uopts scrt = do
  case scrt of
    ["reader"] -> eff genRecvSock (runEffect . echoDoubleSection)
    ["writer"] -> eff genConnSock (runEffect . chamberSection)
    _ -> fail ("Unexpected: " <> show scrt)
  where
    eff f g = withConnectedSock (f $ genIP4Sock (optInetHostAddr uopts) (optInetPortNumber uopts)) g

entryUNIXSock :: MainOptions -> UNIXSockOptions -> [String] -> IO ()
entryUNIXSock _ uopts scrt = do
  case scrt of
    ["reader"] -> eff genRecvSock (runEffect . echoDoubleSection)
    ["writer"] -> eff genConnSock (runEffect . chamberSection)
    _ -> fail ("Unexpected: " <> show scrt)
  where
    eff :: (AddrInfo -> IO Socket) -> (Socket -> IO a) -> IO a
    eff f g = withConnectedSock (f $ genUnixSock $ optUnixSockAddr uopts) g

main :: IO ()
main = runSubcommand
  [ subcommand "unix" entryUNIXSock
  , subcommand "ip4" entryIP4Sock
  ]
{-
main = runSubcommand
  [ subcommand "reader" (entry genRecvSock (runEffect . echoDoubleSection))
  , subcommand "writer" (entry genConnSock (runEffect . chamberSection))
  ]
-- -}
{-
main = runSubcommand
  [ subcommand "reader" (entry genRecvSock (runEffect . readerSection))
  , subcommand "writer" (entry genConnSock (runEffect . writerSection))
  ]
-- -}
-- main = runEffect $ stdinLn >-> (conj >~ stdoutLn)
-- main = runEffect $ lift getLine >~ conj >~ stdoutLn
-- main = runEffect loop


stdoutLnSock :: Consumer' ByteString IO ()
stdoutLnSock = do
  str <- await
  x <- lift $ try $ putStrLn ("> " <> show str)
  case x of
    Right () -> stdoutLnSock
    Left e@G.IOError{ G.ioe_type = t } ->
      lift $ unless ( t == G.ResourceVanished ) (throwIO e)

conj :: Consumer Text IO Text
conj = (<>) <$> await <*> await

loop :: Effect IO ()
loop = for stdinLn $ \str ->
  lift $ putStrLn ("> " <> show str)

stdinLn :: Producer' Text IO ()
stdinLn = do
  eof <- lift isEOF
  unless eof $ do
    str <- lift getLine
    yield str
    stdinLn

txt2bs :: Pipe Text ByteString IO ()
txt2bs = forever $ do
  tx <- await
  bs <- lift $ T.withCStringLen tx BS.packCStringLen
  yield bs

stdoutLn :: Show a => Consumer' a IO ()
stdoutLn = do
  str <- await
  x <- lift $ try $ putStrLn ("> " <> show str)
  case x of
    Right () -> stdoutLn
    Left e@G.IOError{ G.ioe_type = t } ->
      lift $ unless ( t == G.ResourceVanished ) (throwIO e)



stdioLn :: Server' ByteString Text IO ()
stdioLn = do
  eof <- lift isEOF
  unless eof $ do
    txt <- lift getLine
    str <- respond txt -- 下流に流して返事を貰う
    x <- lift $ try $ C8.putStrLn ("> " <> str)
    case x of
      Right () -> stdioLn
      Left e@G.IOError{ G.ioe_type = t } ->
        lift $ unless ( t == G.ResourceVanished ) (throwIO e)



pt2bs :: Text -> Proxy x' Text x' ByteString IO r
pt2bs = bipipeM tbs pure
  where
    tbs txt = T.withCStringLen txt BS.packCStringLen

chamberSection :: Socket -> Effect' IO ()
chamberSection sock = stdioLn >>~ pt2bs >>~ kchamber sock


kchamber :: Socket -> ByteString -> Client' ByteString ByteString IO ()
kchamber sock = next
  where
    next bs = do
      lift $ SocketBS.sendAll sock bs
      r <- lift $ SocketBS.recv sock 4096
      request r >>= next
      -- 上流に返しつつ、次に下流に流れてきたものを受けて再帰


-- request :: a' -> Client' a' a m a
-- respond :: a  -> Server' a' a m a'

-- />/ :: (a  -> Proxy x' x b' b m a')
--     -> (b  -> Proxy x' x c' c m b')
--     -> (a  -> Proxy x' x c' c m a')
-- \>\ :: (b' -> Proxy a' a y' y m b )
--     -> (c' -> Proxy b' b y' y m c )
--     -> (c' -> Proxy a' a y' y m c )
-- >~> :: (_a -> Proxy a' a b' b m r )
--     -> (b  -> Proxy b' b c' c m r )
--     -> (_a -> Proxy a' a c' c m r )
-- >>~ ::        Proxy a' a b' b m r
--     -> (b  -> Proxy b' b c' c m r )
--     ->        Proxy a' a c' c m r
-- >+> :: (b' -> Proxy a' a b' b m r )
--     -> (_c'-> Proxy b' b c' c m r )
--     -> (_c'-> Proxy a' a c' c m r )
-- +>> :: (b' -> Proxy a' a b' b m r )
--     ->        Proxy b' b c' c m r
--     ->        Proxy a' a c' c m r


-- Server' b' b = Proxy x' x b' b
-- Client' a' a = Proxy a' a y' y
-- Producer' b  = Proxy x' x () b
-- Consumer' a  = Proxy () a y' y
-- Pipe a b     = Proxy () a () b

  

-- isUnixDomainSocketAvailable :: Bool
-- isSupportedSocketType :: SocketType -> Bool
-- isSupportedSockAddr :: SockAddr -> Bool
-- socketPair AF_UNIX Stream defaultProtocol


peek :: BiPipe x' ByteString x' ByteString IO r
peek = bipipeM dump pure
  where
    dump bs = do
     BS.hPutBuilder stdout $ BS.byteStringHex bs
     putStrLn ""
     pure bs

double :: BiPipe x' ByteString x' ByteString IO r
double = bipipe dbl id
  where
    dbl x = x <> x

prependUTFString :: String -> BiPipe x' ByteString x' ByteString IO r
prependUTFString str = bipipe prep id
  where
    prep = (<>) (f str) -- OverloadedStringsでの変換だとエンディアンで不都合するっぽい？
    f = BSL.toStrict . BS.toLazyByteString . BS.stringUtf8

echoDoubleSection :: Socket -> Effect' IO ()
echoDoubleSection sock = (fromSocket sock 4096 >>~ peek >>~ double >>~ prependUTFString "も") >-> toSocket sock

writerSection :: Socket -> Effect' IO ()
writerSection sock = stdinLn >-> txt2bs >-> toSocket sock

readerSection :: Socket -> Effect' IO ()
readerSection sock = readerSectionBeg sock >-> stdoutLn

readerSectionBeg :: Socket -> Producer' ByteString IO ()
readerSectionBeg sock = fromSocket sock 4096



unlink :: SockAddr -> IO ()
unlink (SockAddrUnix addr) = removeLink addr
unlink _ = pure ()

genRecvSock :: AddrInfo -> IO Socket
genRecvSock info = do
  unlink (addrAddress info) -- unlink .sock if it exists.
  sock <- socket (addrFamily info) (addrSocketType info) (addrProtocol info)
  bind sock (addrAddress info)
  Socket.listen sock 1 -- up to one connection.
  (conn,_) <- Socket.accept sock -- 接続待ちsocketと確立したconnection用のsocketは別instance
  pure conn

genConnSock :: AddrInfo -> IO Socket
genConnSock info = do
  sock <- socket (addrFamily info) (addrSocketType info) (addrProtocol info)
  Socket.connect sock (addrAddress info)
  pure sock

withConnectedSock :: IO Socket -> (Socket -> IO a) -> IO a
withConnectedSock f = bracket f (\s -> shutdown s ShutdownBoth >> close s)
-- close :: Socket -> IO () -- with no exceptions
-- close' :: Socket -> IO () -- with exceptions

genUnixSock :: String -> AddrInfo
genUnixSock addr =
  defaultHints { addrFamily = AF_UNIX
               , addrSocketType = Stream
               , addrAddress = SockAddrUnix addr
               }

genIP4Sock :: HostAddress -> PortNumber -> AddrInfo
genIP4Sock addr port =
  defaultHints { addrFamily = AF_INET
               , addrSocketType = Stream
               , addrAddress = SockAddrInet port addr
               }


unixsock :: AddrInfo
unixsock = defaultHints { addrFamily = AF_UNIX
                 , addrSocketType = Stream
                 , addrAddress = SockAddrUnix "/tmp/sock-test.sock"
                 }

loopback :: AddrInfo
loopback =  defaultHints { addrFamily = AF_INET
                 , addrSocketType = Stream
                 , addrAddress = SockAddrInet 33333 (tupleToHostAddress (0x7f,0,0,0x01))
                 }
